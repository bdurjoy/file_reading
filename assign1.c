#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#define MAX_LINE_LENGTH 10485
#define MAX_WORD_LENGTH 1048576


typedef struct node{
    char *word;
    int wordReferenceCount;
    struct node *nextWord;
}Node;

Node *createNewNode(){
    Node *pNode = (Node*) malloc(sizeof(Node));
    return pNode;
}

void display(Node *pHead)
{
    if(pHead != NULL){

        Node *pNode = pHead;

        while(pNode != NULL){

            printf("Word\t\t\t\t\t:%s\n", pNode->word);
            pNode = pNode->nextWord;
        }
    }
}

void appendNode(char *word, Node **ppList, Node *pNewNode) {
    pNewNode->word = malloc(strlen(word)+1);
    strcpy(pNewNode->word, word);
    pNewNode->nextWord = NULL;

    if(*ppList == NULL)
        *ppList = pNewNode;
    else {
        Node *temporaryList = *ppList;
        for(; temporaryList->nextWord!=NULL; temporaryList=temporaryList->nextWord);
        temporaryList->nextWord = pNewNode;
    }
}


int findWords(char* word_buffer, Node **ppHead){
    Node* current = *ppHead;  /* Initialize current*/
    while (current != NULL)
    {
        if (strcmp(current->word,word_buffer)==0)
            return 0;
        current = current->nextWord;
    }
    return 1;
}

void minimumWordLenth(Node *pHead, int wordLength){

    if(pHead != NULL){
        Node *temporaryNode = pHead;

        while(temporaryNode != NULL){
            if(wordLength <= (signed)strlen(temporaryNode->word)){
                printf("Word\t\t\t\t\t:%s\n", temporaryNode->word);
            }
            temporaryNode = temporaryNode->nextWord;
        }
    }

}

void maxWordLength(Node *pHead, int wordLength){
    if(pHead != NULL){
        Node *temporaryNode = pHead;

        while(temporaryNode != NULL){
            if(wordLength > (signed)strlen(temporaryNode->word)){
                printf("Word\t\t\t\t\t:%s\n", temporaryNode->word);
            }
            temporaryNode = temporaryNode->nextWord;
        }
    }
}

void exactWordLenth(Node *pHead, int wordLength){
    if(pHead != NULL){
        Node *temporaryNode = pHead;

        while(temporaryNode != NULL){
            if(wordLength == (signed)strlen(temporaryNode->word)){
                printf("Word\t\t\t\t\t:%s\n", temporaryNode->word);
            }
            temporaryNode = temporaryNode->nextWord;
        }
    }

}

void flush(){
    while(getchar() != '\n');
}
/*
void saveList(Node *pList, FILE *fp, char *file_name){
    Node *temporary = pList;
    fp = fopen(file_name, "w");

    while(temporary != NULL){
        fprintf(fp, "%s", temporary->word);
        temporary = temporary->nextWord;
    }

}*/

int main(int argc, char *argv[]){

    int LineProcessed = 0;/*counts the line length*/
    int numCounts;/*counts character in the word*/
    char *p;
    int wordLength = 3;/*word length sets as min 3 character*/
    int WordsBelowLength = 0;
    int WordsAccepted = 0;/*Word accepted in the linked-list*/
    int WordsProcessed = 0;
    int width = 4;
    int character, count;
    int userInput;



    Node *pHead = NULL;
    char file_buffer[MAX_LINE_LENGTH];
	char *input_buffer = file_buffer;
	char word_buffer[MAX_WORD_LENGTH];
	/*char* file_name;*/
	FILE *fp;

	if(argc >= 3){
        wordLength = atoi( argv[2] );/*takes the min word length from the command line*/
    }

    if (argc >= 2){
        fp = fopen(argv[1], "r");

		if(fp == NULL || !fp){
			fprintf(stderr, "File %s not found. Terminating.\n", argv[1]);
			fclose(fp);
			return 0;
		}
	}else{
			fprintf(stderr, "<filename> missing\n");
			fprintf(stderr, "Usage\t\t\t\t\t:./assign1 <filename>\n");
			return 0;
	}




    while(fgets(input_buffer, MAX_LINE_LENGTH, fp)){
        p = input_buffer;
        for ( ; *p ; p++) *p = tolower(*p);/*transform the word into lowercase*/
		LineProcessed++;
        numCounts = 0;

		while(sscanf(input_buffer,"%s%n", word_buffer, &numCounts)== 1){

            character = 0;
            count = 0;
            while (word_buffer[count] != '\0'){
                if (!(word_buffer[count] == ' ' && word_buffer[count+1] == ' ')) {
                    word_buffer[character] = word_buffer[count];
                    character++;
                }
                count++;
            }

            word_buffer[character] = '\0';
            WordsProcessed++;
            if(character <= wordLength){

                  printf("Word found less than %d characters\t:%-*s\n", wordLength, width, word_buffer);
                  WordsBelowLength++;

            }else{
                if(findWords(word_buffer, &pHead)==1){
                    appendNode(word_buffer, &pHead, createNewNode());
                    WordsAccepted++;
                }else{
                    printf("Duplicate word found\t\t\t:%-*s\n", width, word_buffer);
                }

            }

            input_buffer += numCounts;

            }

		}
    do{
        printf("1. Display statistics about the processed words.\n");
        printf("2. List all the words found in ascending sorted order.\n");
        printf("3. List all the words found greater than a certain length.\n");
        printf("4. List all the words found less than a certain length.\n");
        printf("5. List all the words found equal to a certain length.\n");
        printf("6. Write all the words found to a file.\n");
        printf("9. Exit program.\n");

        if(scanf("%d", &userInput) != 1){
            flush();
        }


        switch(userInput){
        case 1:
            fprintf(stdout,"Input filename\t\t\t\t:%-*s\n", width, argv[1]);
            printf("Lines Processed\t\t\t\t:%-*d\n", width, LineProcessed);
            printf("Words Processed\t\t\t\t:%-*d\n", width, WordsProcessed);
            printf("Words stored in linked list\t\t:%-*d\n", width, WordsAccepted);
        break;
        case 2:
            display(pHead);
            break;
        case 3:
            printf("Enter the minimum word Length\t\t:");
            if(scanf("%d", &wordLength) != 1){
                flush();
            }else{
                minimumWordLenth(pHead, wordLength);
            }
            break;
        case 4:
            printf("Enter the maximum word Length\t\t:");
            if(scanf("%d", &wordLength) != 1){
                flush();
            }else{
                maxWordLength(pHead, wordLength);
            }
            break;
        case 5:
            printf("Enter the exact word Length\t\t:");
            if(scanf("%d", &wordLength) != 1){
                flush();
            }else{
                exactWordLenth(pHead, wordLength);
            }
            break;
        case 6:
            /*Write into file
            printf("Enter the file Name\t\t\t:");
            scanf("%s",file_name);
            saveList(pHead, fp, file_name);
            */
            break;
        case 9:
            printf("Goodbye!");
            break;
        default:
            printf("Input Invalid! Try Again.\n");
            break;
        }

    }while(userInput != 9 && userInput != EOF);


    fclose(fp);


    return 0;


}






