CC = gcc
CC_FLAGS = -g -pedantic -Wall -W -std=c89

FILES = assign1.c 
OUT_EXE = assign1

build: $(FILES)
	$(CC) $(CC_FLAGS) -o $(OUT_EXE) $(FILES) 

clean:
	rm -f *.o core *.exe *~ *.out *.stackdump

rebuild: clean build
